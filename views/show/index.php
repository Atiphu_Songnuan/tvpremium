<!-- ================================= Created by Famz ==================================== -->
<!-- | ========================== Created Date: 18 Sep 2019 ============================= | -->
<!-- ================================= Modified Step ====================================== -->
<!-- | * เปลี่ยน path เรียก queue ใน function listAllQueue จาก "tv" เป็น "testtv" or something | -->
<!-- | * เอา controls ออกจาก video เมื่อขึ้น production | -->
<!-- ====================================================================================== -->

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- <meta charset="UTF-8"> -->
        <link rel="icon" href="public/img/premium.png">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet"
            href="public/css/bootstrap.min.css">

        <link href="https://fonts.googleapis.com/css?family=K2D&display=swap" rel="stylesheet">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->


        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/popper.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
        <title>Videos</title>

        <style>
            * {
                box-sizing: border-box;
            }

            body {
                margin: 0;
                font-family: 'K2D', sans-serif;
                font-size: 17px;
            }

            .video{
                position: fixed;
                width: 100%;
                height: 100%;
                /* background: green; */
                background-image: url('public/img/moror.jpg');
                background-repeat: no-repeat;
                /* background-attachment: fixed; */
                background-size: 100% 100%;
            }

            #myVideo {
                /* position: fixed; */
                /* right: 0;
                top: 0;
                min-width: 100%;
                min-height: 90%; */

                width: 100% !important;
                height: 100% !important;
                right: 0;
                top: 0;
                min-width: 100%;
                min-height: 100%;
                object-fit: fill;
            }

            .content {
                position: fixed;
                bottom: 0;
                /* background: rgba(0, 0, 0, 0.5); */
                /* background: rgb(2,0,36); */
                /* background: #1c8445; */
                background: #fffda6;
                /* background: white; */
                color: #f1f1f1;
                width: 100%;
                height: 15%;
                /* padding: 25px; */
            }

            .clock{
                font-family: 'K2D', sans-serif;
                position: fixed;
                right: 0;
                top: 0;
                /* background: rgba(0, 0, 0, 0.5); */
                /* color: #f1f1f1; */
                width: 20%;
                padding: 20px;
                font-size: 80px;
            }

        </style>

    </head>
    <body>
        <div class="video" id="videoContent">
            <video muted autoplay id="myVideo">
                <source id="vdosource">
            </video>
        </div>
<!-- 
        <div class="clock text-right">
            <p id="digitaltime" class="text-white badge badge-pill badge-danger">00:00:00</p>
        </div>

        <div class="content">
            <div class="align-items-center mt-3" id="queuelist">
            </div>
        </div> -->
    </body>


    <script>
        var curVideo = 0;
        var ended = false;

        $(function(){
            playVideo();
            var vid = document.getElementById("myVideo");
            vid.addEventListener("ended", function(){
                curVideo++;
                nexVideo(curVideo);
            })
        });

        // function refreshTime() {
        //     var options = { timeZone: 'Asia/Bangkok', hour12: false };
        //     var dateString = new Date().toLocaleTimeString("en-US", options);
        //     var formattedString = dateString.replace(", ", " - ");
        //     $('#digitaltime').text(formattedString);
        // }
        // setInterval(refreshTime, 1000);

        function playVideo(){
            nexVideo(curVideo);
        }

        function nexVideo(index){
            //Playlist
            // var videos = [  "Mor-Or-Care-Plus.mp4", "vid20.mp4","vid19.mp4","vid21.mp4","vid22.mp4",
            //                 "vid3.mp4","vid9.mp4","vid18.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid4.mp4",
            //                 "vid8.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid11.mp4",
            //                 "vid1.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid10.mp4",
            //                 "vid2.mp4","vid20.mp4","vid19.mp4","vid21.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid5.mp4",
            //                 "vid6.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid7.mp4",
            //                 "vid12.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid13.mp4",
            //                 "vid14.mp4","vid20.mp4","vid19.mp4","vid21.mp4","vid22.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid15.mp4",
            //                 "vid16.mp4",
            //                 "Mor-Or-Care-Plus.mp4", "vid17.mp4"];


            // var videos = ["vid27.mp4", "vid26.mp4", "vid25.mp4", "vid19.mp4", "vid24.mp4", "vid23.mp4",
            //               "vid20.mp4", "vid22.mp4", "vid16.mp4", "vid1.mp4", "vid12.mp4",
            //               "vid13.mp4", "vid14.mp4", "vid15.mp4", "vid16.mp4", "vid18.mp4",
            //               "vid3.mp4", "vid4.mp4", "Mor-Or-Care-Plus.mp4"];

            // Covid-19 Playlist
            var videos = ["prpremium.mp4"];
            // var videos = ["vid15.mp4","vid16.mp4","vid17.mp4"];
            var vdosource = document.getElementById("myVideo");
            if(index < videos.length){
                // vdosource.src = "http://61.19.201.20:19539/videoontv/public/src/premium/" + videos[index];
                vdosource.src = "https://hosapp01.medicine.psu.ac.th/videoontv/public/src/premium/" + videos[index];
                vdosource.type = "video/mp4";

            } else if (index == videos.length){
                console.log("Repeat Playlist");
                resetVideo();
                playVideo();
            }
        }

        function resetVideo(){
            curVideo = 0;
            ended = true;
        }

        let connection = true
        setInterval(function() {
            if (!navigator.onLine) {
                connection = false;
                console.log("Connection Lost");
                // console.log(navigator.onLine)
            } else{
                if (!connection) {
                    console.log("Refresh");
                    location.reload();
                }
            }
        }, 1000);

   </script>
</html>
