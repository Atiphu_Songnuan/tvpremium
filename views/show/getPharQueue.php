<?php
    $URL = "https://his01.psu.ac.th/HosApp/testtv/views/show/PharQ.php";

    //Initiate cURL.
    $ch = curl_init($URL);

    //Disable CURLOPT_SSL_VERIFYHOST and CURLOPT_SSL_VERIFYPEER by
    //setting them to false.
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    //Execute the request.
    curl_exec($ch);

    //Check for errors.
    if (curl_errno($ch)) {
        throw new Exception(curl_error($ch));
    }
?>