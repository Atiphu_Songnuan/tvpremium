<?php
class Show extends Controller
{
    public function __construct()
    {
        parent::__construct('show');

        $this->views->GetQueue = $this->model->GetQueue();
    }

    public function index()
    {
        $this->views->render('show/index');
    }

}
