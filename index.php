<?php
//Use an autoload.
// error_reporting(0);
require_once 'libs/ApiService.php';
require_once 'libs/Router.php';
require_once 'libs/Controller.php';
require_once 'libs/Model.php';
require_once 'libs/View.php';

//Call library
require_once 'libs/Database.php';

// require_once 'config/paths.php';
require_once 'config/database.php';

$app = new Router();
